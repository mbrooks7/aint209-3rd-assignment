﻿using UnityEngine;
using System.Collections;

public class ExitGame : MonoBehaviour {

    public void ClickExit()
    {
        Debug.Log("Game is Exiting...");
        Application.Quit();
    }
}
