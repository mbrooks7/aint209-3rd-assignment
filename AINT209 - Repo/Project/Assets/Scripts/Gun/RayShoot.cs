﻿using UnityEngine;
using System.Collections;

public class RayShoot : MonoBehaviour 
{
    public int clip = 3;
    public int bulletsPerClip = 30;
    public int bulletsLeft = 0;
    public int bulletsTotal;
    public int particleSpeed = 200000;
    

    public float damage = 10.0f;
    public float range = 1000.0f;
    public float reloadTime = 1.6f;
    public float force = 1000;
    public float fireSpeed = 15.0f;

    public GameObject bullet;
    public GameObject bulletSpawn;
    public GameObject bulletHole;
    
    [HideInInspector]
    public float waitTillFire = 0;
    
    public float shootTimer = 0.0f;
    public float shootCooler = 0.1f;
    
    public float muzzleFlashCooler = 0.1f;
    public float muzzleFlashTimer = 0.0f;

    public float keyCooler = 0.5f;
    public float keyTimer = 0.0f;

   // public GameObject light1;
   // public GameObject light2;
  //  public GameObject light3;

    public AudioClip reloadSound;
    public AudioClip shootSound;
    public AudioClip emptyGunSound;

    public ParticleEmitter muzzleFlash;
   // public ParticleEmitter hitParticles;

    
	// Use this for initialization
	void Start () 
    {
        bulletsTotal = bulletsPerClip * clip; 
        bulletsLeft = bulletsPerClip;
      //  hitParticles.emit = false;
        muzzleFlash.gameObject.SetActive(true);
        muzzleFlash.emit = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (keyTimer > 0)
        {
            keyTimer -= Time.deltaTime;
        }

        if (keyTimer < 0)
        {
            keyTimer = 0;
        }

        if (muzzleFlashTimer > 0)
        {
            muzzleFlashTimer -= Time.deltaTime;
            MuzzleFlashShow();
        }

        if (muzzleFlashTimer < 0)
        {
            muzzleFlashTimer = 0;
        }

        if (shootTimer > 0)
        {
            shootTimer -= Time.deltaTime;
        }

        if (shootTimer < 0)
        {
            shootTimer = 0;
        }

        if (keyTimer == 0)
        {
            if (Input.GetMouseButton (0) && bulletsLeft > 0)
            {
                if (shootTimer == 0)
                {
                    PlayShootAudio();
                    RayFire();
                    Instantiate(bullet, bulletSpawn.transform.position, transform.rotation);

                    shootTimer = shootCooler;
                    keyTimer = keyCooler;            
                }

                if (muzzleFlashTimer == 0)
                {
                    muzzleFlashTimer = muzzleFlashCooler;
                    MuzzleFlashShow();
                }
            }
            else if (Input.GetMouseButtonDown (0) && clip == 0)
            {
                PlayEmptyAudio();
                shootTimer = shootCooler;
                keyTimer = keyCooler;
            }
            if (Input.GetKey(KeyCode.R))
            {            
                Reload();
                shootTimer = shootCooler;
                keyTimer = keyCooler;
            }
        }
	}

    void MuzzleFlashShow()
    {
        if (muzzleFlashTimer > 0)
        {
            muzzleFlash.emit = false;
           // light1.SetActive(false);
          //  light2.SetActive(false);
          //  light3.SetActive(false);
        }

        if (muzzleFlashTimer == muzzleFlashCooler)
        {
            muzzleFlash.transform.localRotation = Quaternion.AngleAxis(Random.value * 360 * particleSpeed, Vector3.forward);
            muzzleFlash.emit = true;
         //   light1.SetActive(true);
         //   light2.SetActive(true);
         //   light3.SetActive(true);
        }
    }

    void RayFire()
    {
        GameObject.Find("CQAssaultRifle").animation.Play("GunFiring"); // First name is for name of object that will play your animation and second name is what you animation is called

        RaycastHit hit;
        Vector3 directionRay = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(transform.position, directionRay * range, Color.yellow);
        
        if (Physics.Raycast(transform.position, directionRay, out hit, range))
        {
            if (hit.rigidbody)
            {
                // if (hitParticles)
                // {
                //      hitParticles.transform.position = hit.point;
                //      hitParticles.transform.localRotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
                //      hitParticles.Emit();

                hit.rigidbody.AddForceAtPosition(directionRay * force, hit.point);

                //      hit.collider.SendMessageUpwards("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
                // }
            } 
        }
        
        bulletsLeft--;

        if (bulletsLeft < 0)
        {
            bulletsLeft = 0;
        }        
        if (bulletsLeft == 0)
        {
            Reload();
        }            
    }
    void PlayShootAudio()
    {
        audio.PlayOneShot(shootSound);
    }

    void PlayReloadAudio()
    {        
        audio.PlayOneShot(reloadSound);
    }

    void PlayEmptyAudio()
    {
        audio.PlayOneShot(emptyGunSound);
    }

    void Reload()
    {
        if (clip > 0)
        {
            StartCoroutine(ReloadSpeed());
            clip--;
        }        
    }

    IEnumerator ReloadSpeed()
    {
        if (clip > 0)
        {
            // GameObject.Find("").animation.Play(""); // First name is for name of object that will play your animation and second name is what you animation is called
            PlayReloadAudio();
            yield return new WaitForSeconds(reloadTime);

            //if (clip > 0)
            //{
            bulletsLeft = bulletsPerClip;
            //}
        }
        else if (clip < 0)
        {
            PlayEmptyAudio();
        }
    }   
}
