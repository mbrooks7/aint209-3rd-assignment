﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour 
{
    public float maxDist;
    public GameObject decalHitWall;
    public float floatInFrontOfWall;

    
    


	// Use this for initialization
	void Start () 
    {
        maxDist = 1000000000.0f;
       floatInFrontOfWall = 0.00001f;
       
	}
	
	// Update is called once per frame
	void Update () 
    {
        RaycastHit hit;
        Quaternion rayObjectRotation;

        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDist))
        {
            if (decalHitWall && hit.transform.tag == "Level Parts")
            {
                Instantiate(decalHitWall, hit.point + (hit.normal * floatInFrontOfWall),
                    Quaternion.LookRotation(hit.normal));
            }
            Destroy(gameObject);
        }
	}
}