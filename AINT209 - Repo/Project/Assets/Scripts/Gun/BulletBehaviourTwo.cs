﻿using UnityEngine;
using System.Collections;

public class BulletBehaviourTwo : MonoBehaviour {
    
	//Hiding these 2 variables
	[HideInInspector]
	public float lifeTime = 2.0f;
	[HideInInspector]
	public int bulletSpeed = 5;

	
	// Use this for initialization
	void Start()
	{
		//StartCoroutine (OnTriggerEnter(collider));
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(Vector3.down * Time.deltaTime * bulletSpeed);
        rigidbody.AddForce(transform.forward * 3000);
	}

/*	IEnumerator OnTriggerEnter(Collider collider)
	{
	//	if (collider.gameObject.tag == "disappearing_wall") 
		//{
			//audio.PlayOneShot (disappearingWall);
			//while (true)
			//{
			//	Destroy (this);
			//	yield return null;
			//}
		///}

		if (collider.gameObject.tag == "reappearing_wall") 
		{
			audio.PlayOneShot (reappearingWall);
			while (true)
			{
				Destroy (this);
				yield return null;
			}
		}
	}
*/
    //void Awake()
  //  {
   //     Destroy(gameObject, lifeTime);
   // }	
}
