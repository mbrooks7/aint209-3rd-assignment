﻿using UnityEngine;
using System.Collections;

public class HashIDs : MonoBehaviour 
{
    public int dyingState;
    public int locomotionStateWalk;
    public int crouchState;
    public int aimWalkState;
    public int aimJogState;
    //public int locomotionStateJog;

    public int speedFloat;
    public int crouchingBool;
    public int aimingBool;
    public int aimWalkingBool;
    public int aimRunningBool;
    public int deadBool;

    void Awake()
    {
        dyingState = Animator.StringToHash("Base Layer.Dead");
        deadBool = Animator.StringToHash("Dead");

        locomotionStateWalk = Animator.StringToHash("Base Layer.Locomotion - Walk");
     //   locomotionStateJog = Animator.StringToHash("Base Layer.Locomotion - jog");
        crouchState = Animator.StringToHash("Base Layer.Locomotion - Crouch");
        aimWalkState = Animator.StringToHash("Base Layer.Aiming");
        aimJogState = Animator.StringToHash("Base Layer.MIL2_M3_W2_Jog_Aim_F_Loop");

        speedFloat = Animator.StringToHash("Speed");
        crouchingBool = Animator.StringToHash("Crouching");
        aimingBool = Animator.StringToHash("Aiming");
        aimWalkingBool = Animator.StringToHash("Aiming Walking");
        aimRunningBool = Animator.StringToHash("Aiming Running");

    }
}
