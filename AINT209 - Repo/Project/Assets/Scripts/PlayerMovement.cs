﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
    public float turnSmoothing = 15f;
    public float speedDampTime = 0.1f;

    private Animator anim;
    private HashIDs hash;

    void Awake()
    {
        anim = GetComponent<Animator>();
        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
       // anim.SetLayerWeight(1, 1f);
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        bool crouch = Input.GetButton("Crouch");
    }


    void MovementManagement(float horizontal, float vertical, bool crouch)
    {
        anim.SetBool(hash.crouchingBool, crouch);

        if (horizontal != 0f || vertical != 0f)
        {
            Rotation(horizontal, vertical);
            anim.SetFloat(hash.speedFloat, 5.5f, speedDampTime, Time.deltaTime);
        }
        else
        {
            anim.SetFloat(hash.speedFloat, 0f);
        }

        
    }

    void Rotation(float horizontal, float vertical)
    {
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        rigidbody.MoveRotation(newRotation);
    }

  /*  void AudioManagement()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).nameHash == hash.locomotionStateWalk)
        {
            if (!audio.isPlaying)
            {
                audio.Play();
            }
        }
        else
        {
            audio.Stop();
        }
    }
*/
    
}
