﻿using UnityEngine;
using System.Collections;

public class Teleporter2 : MonoBehaviour
{
    public Transform teleporterPoint1;
    public Transform teleporterPoint2;
	
    void OnTriggerEnter(Collider collider)
    {
		if (collider.tag == "Teleporter2 loc1") 
		{
			transform.position = teleporterPoint1.position;
		}
		if (collider.tag == "Teleporter2 loc2") 
		{
			transform.position = teleporterPoint2.position;
		}
        if (collider.tag == "Teleporter2 loc3")
        {
            transform.position = teleporterPoint1.position;
        }
	}
}
