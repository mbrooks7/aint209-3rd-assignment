﻿using UnityEngine;
using System.Collections;

public class changeGun : MonoBehaviour 
{
    //Primary Weapon
    public  GameObject  gun;         //Model for Gun    

    //Secondary Weapon
    public  GameObject  gravityGun; //Model for Gravity Gun
    public  GameObject  pickTo;     //Script for Gravity Gun
    public  GameObject  gunLight;
    public bool lightOn = false;
    
    void Start()
    {
        gunLight.gameObject.SetActive(false);
        //Disables Model for Gravity Gun on Start
        gravityGun.gameObject.SetActive(false);
        
        //Disables Script for Gravity gun on Start
        pickTo.gameObject.GetComponent<PickUp>().enabled = false;

        //Enables Model for Gun on Start
        gun.gameObject.SetActive(true);     
    }   
    	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.F) && lightOn == false)
        {
            lightOn = true;
            gunLight.gameObject.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.F) && lightOn == true)
        {
            lightOn = false;
            gunLight.gameObject.SetActive(false);
        }
        
        if (Input.GetAxis("Mouse ScrollWheel") < 0 || Input.GetKeyDown("1"))
        {
            //Changes guns
            //I will probably add an animation showing the change just before here.

            //Disables Model for Gravity Gun
            gravityGun.gameObject.SetActive(false);

            //Disables Script for Gravity Gun
            pickTo.gameObject.GetComponent<PickUp>().enabled = false;

            //Enables Model for Gun
            gun.gameObject.SetActive(true);          
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetKeyDown("2"))
        {
            //Changes guns
            //I will probably add an animation showing the change just before here

            //Enables Model for Gravity Gun
            gravityGun.gameObject.SetActive(true);

            //Enables Script for Gravity Gun
            pickTo.gameObject.GetComponent<PickUp>().enabled = true;

            //Disables Model for Gun
            gun.gameObject.SetActive(false);

        }	
    }
}
