﻿using UnityEngine;
using System.Collections;

public class PickedUpObject : MonoBehaviour {

    public bool refuseThrow = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Player" && other.gameObject.tag != "pickto")
        {
            refuseThrow = true;
        }
    }

    void OneTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player" && other.gameObject.tag != "pickto")
        {
            refuseThrow = false;
        }
    }
}
