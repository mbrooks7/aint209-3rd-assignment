﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {

    public Quaternion   objectRot;
    public Vector3      objectPos;

    public GameObject   pickObj;
    public GameObject   pickref;

    public bool         canpick = true;
    public bool         picking = false;

  	// Use this for initialization
	void Start ()
    {
        pickref = GameObject.FindWithTag("pickupref");
        pickObj = pickref;
	}

	// Update is called once per frame
	void Update ()
    {
        objectPos = transform.position;

        objectRot = transform.rotation;

        //Method here picks up the object with the right mouse button
        if (Input.GetMouseButtonDown (1) && canpick)
		{
			Debug.Log ("Object Picked Up");
			

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 10) && hit.collider.gameObject.tag == "pickup")
            {
                picking = true;
                pickObj = hit.collider.gameObject;
                hit.rigidbody.useGravity = false;
                hit.rigidbody.isKinematic = true;

                hit.transform.parent = gameObject.transform;
                hit.transform.position = objectPos;
                hit.transform.rotation = objectRot;

            }
		}
		
        if (Input.GetMouseButtonUp (1) && picking)
		{
			picking = false;
			canpick = false;
		}

        //Method here fires the object if it has been picked up by clicking the left mouse button
        if (Input.GetMouseButtonDown(0) && !canpick && pickObj.GetComponent<PickedUpObject>().refuseThrow != true)
        {
            canpick = true;
            pickObj.rigidbody.useGravity = true;
            pickObj.rigidbody.isKinematic = false;
            pickObj.transform.parent = null;
            
            pickObj.rigidbody.AddForce(transform.forward * 3000);
            pickObj = pickref;
        }

        //Method here drops the object if it has been picked up by clicking the right mouse button
        if (Input.GetMouseButtonDown (1) && !canpick && pickObj.GetComponent<PickedUpObject> ().refuseThrow != true)
		{
			canpick = true;
			pickObj.rigidbody.useGravity = true;
			pickObj.rigidbody.isKinematic = false;
			pickObj.transform.parent = null;
			
			pickObj = pickref;
		}

    }
}
