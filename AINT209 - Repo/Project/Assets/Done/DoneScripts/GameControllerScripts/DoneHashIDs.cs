using UnityEngine;
using System.Collections;

public class DoneHashIDs : MonoBehaviour
{
	// Here we store the hash tags for various strings used in our animators.
	public int dyingState;
    public int deadBool;

	public int locomotionState;
    public int speedFloat;

    public int crouchingState;
    public int crouchingBool;

    public int crouchWalkingState;
    public int crouchWalkingBool;

    public int sprintingState;
    public int sprintBool;

    public int aimWalkingState;
    public int aimWalkBool;

    public int aimJoggingState;
    public int aimJogBool;

    public int reloadState;
    public int reloadBool;

    public int changeGunState;
    public int changeGunBool;

	//public int shoutState;
	//public int shoutingBool;

	//public int playerInSightBool;
	//public int shotFloat;
	public int aimWeightFloat;
    public int angularSpeedFloat;
	public int openBool;

	void Awake ()
	{
        speedFloat = Animator.StringToHash("Speed");
        locomotionState = Animator.StringToHash("Base Layer.Locomotion");
        
        sprintingState = Animator.StringToHash("Base Layer.Running");
        sprintBool = Animator.StringToHash("Sprinting");

        crouchingState = Animator.StringToHash("Base Layer.Crouching");
        crouchingBool = Animator.StringToHash("Crouching");

        crouchWalkingState = Animator.StringToHash("Base Layer.CrouchWalking");
        crouchWalkingBool = Animator.StringToHash("CrouchWalk");

		dyingState = Animator.StringToHash("Base Layer.Dying");
        deadBool = Animator.StringToHash("Dead");

        aimWalkingState = Animator.StringToHash("Base Layer.AimWalking");
        aimWalkBool = Animator.StringToHash("AimWalk");

        aimJoggingState = Animator.StringToHash("Base Layer.AimJogging");
        aimJogBool = Animator.StringToHash("AimJog");

        reloadState = Animator.StringToHash("Base Layer.Reload");
        reloadBool = Animator.StringToHash("Reloading");

        changeGunState = Animator.StringToHash("Base Layer.ChangingGun");
        changeGunBool = Animator.StringToHash("ChangingGun");

		//shoutState = Animator.StringToHash("Shouting.Shout");
		//shoutingBool = Animator.StringToHash("Shouting");

		//playerInSightBool = Animator.StringToHash("PlayerInSight");
		//shotFloat = Animator.StringToHash("Shot");

		aimWeightFloat = Animator.StringToHash("AimWeight");
        angularSpeedFloat = Animator.StringToHash("AngularSpeed");
		openBool = Animator.StringToHash("Open");
	}
}
