using UnityEngine;
using System.Collections;

public class DonePlayerMovement : MonoBehaviour
{

	public float turnSmoothing = 15f;	// A smoothing value for turning the player.
	public float speedDampTime = 0.1f;	// The damping for the speed parameter


	private Animator anim;				// Reference to the animator component.
	private DoneHashIDs hash;			// Reference to the HashIDs.


	void Awake ()
	{
		// Setting up the references.
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneHashIDs>();

		// Set the weight of the shouting layer to 1.
		//anim.SetLayerWeight(1, 1f);
	}


	void FixedUpdate ()
	{
		// Cache the inputs.
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		bool crouch = Input.GetButton("Crouch");
        bool crouchWalk = Input.GetButton("Crouch");
        bool sprinting = Input.GetButton("Run");
        bool aimWalk = Input.GetButton("Aim");
        bool aimJog = Input.GetButton("Aim");
        bool reloading = Input.GetButton("Reloading");
        bool changingGun = Input.GetButton("ChangingGun");

		MovementManagement(h, v, crouch, crouchWalk, sprinting, aimWalk, aimJog, reloading, changingGun);
	}


	void Update ()
	{

	}


	void MovementManagement (float horizontal, float vertical, bool crouching,
        bool crouchWalking, bool sprint, bool aimWalking, bool aimJogging, bool reload, bool changeGun)
	{
        anim.SetBool(hash.sprintBool, sprint);

        anim.SetBool(hash.aimWalkBool, aimWalking);

        anim.SetBool(hash.aimJogBool, aimJogging);

        anim.SetBool(hash.reloadBool, reload);

        anim.SetBool(hash.changeGunBool, changeGun);

		// Set the crouching parameter to the crouch input.
		anim.SetBool(hash.crouchingBool, crouching);

        // Set the crouchWalking parameter to the crouch input.
        anim.SetBool(hash.crouchWalkingBool, crouchWalking);

		// If there is some axis input...
		if(horizontal != 0f || vertical != 0f)
		{
			// ... set the players rotation and set the speed parameter to 5.5f.
		//	Rotating(horizontal, vertical);
			anim.SetFloat(hash.speedFloat, 5.5f, speedDampTime, Time.deltaTime);
		}
		else
			// Otherwise set the speed parameter to 0.
			anim.SetFloat(hash.speedFloat, 0);
	}

	/*
	void Rotating (float horizontal, float vertical)
	{
		// Create a new vector of the horizontal and vertical inputs.
		Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);

		// Create a rotation based on this new vector assuming that up is the global y axis.
		Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

		// Create a rotation that is an increment closer to the target rotation from the player's rotation.
		Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);

		// Change the players rotation to this new rotation.
		rigidbody.MoveRotation(newRotation);
	}
	*/


}
