﻿using UnityEngine;
using System.Collections;

public class Disappear : MonoBehaviour {

    public float lifeTime = 5.0f;
    private float a = 1.0f;

	// Update is called once per frame
	void Update () 
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime < 0)
        {
            a -= (Time.deltaTime / 3f);
            Color textureColor = renderer.material.color;
            textureColor.a = a;
            renderer.material.color = textureColor;

            if (a < 0)
            {
                Destroy(transform.parent.gameObject);
            }
        }
	}
}
