﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    public GameObject bullet;
    public GameObject bulletSpawn;
    public float delayTime = 0.11f;
    public GameObject bulletHole;

    private float counter = 0.0f;

	// Use this for initialization
	void Start () 
    {
        	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    { //makes bullet and fires it.
        if (Input.GetKey(KeyCode.Mouse0) && counter > delayTime)
        {
            Instantiate(bullet, bulletSpawn.transform.position, transform.rotation);
            counter = 0;

            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.forward);


            //Making bullet hole
            if (Physics.Raycast(ray, out hit, 100f))
            {
                Instantiate(bulletHole, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            }

        }
        counter += Time.deltaTime;
	
	}
}
