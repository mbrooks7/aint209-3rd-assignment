﻿using UnityEngine;
using System.Collections;

public class MoveBullet : MonoBehaviour {

    public float speed = 1.0f;
    public float lifeTime = 2.0f;

    public GameObject bulletHole;


	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(0, 0, speed);
	}

    void Awake()
    {
        Destroy(gameObject, lifeTime);
    }

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Level Parts")
        {
            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.forward);

            if (Physics.Raycast(ray, out hit, 100f))
            {
                Instantiate(bulletHole, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            }
            Destroy(c.gameObject);
        }
    }

        
    
}
