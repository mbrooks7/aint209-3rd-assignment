﻿using UnityEngine;
using System.Collections;

public class camSwitch : MonoBehaviour {

	public Camera fpsCamera;
	public Camera tpsCamera;

	public GameObject firstPlayer;
	public GameObject thirdPlayer;

	private bool cameraSwitch = false;
	// Use this for initialization
	void Start () 
	{
		tpsCamera.camera.enabled = true;
		fpsCamera.camera.enabled = false;

		thirdPlayer.gameObject.SetActive(true);
		firstPlayer.gameObject.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Q)) 
		{
			cameraSwitch = !cameraSwitch;
		} 
		if (cameraSwitch == true) 
		{
			tpsCamera.camera.enabled = false;
			fpsCamera.camera.enabled = true;

			thirdPlayer.gameObject.SetActive (false);
			firstPlayer.gameObject.SetActive (true);
		} 
		else 
		{
			tpsCamera.camera.enabled = true;
			fpsCamera.camera.enabled = false;

			thirdPlayer.gameObject.SetActive(true);
			firstPlayer.gameObject.SetActive(false);
		}

	}
}
