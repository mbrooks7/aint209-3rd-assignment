# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is arena based game I was working on using 1st person & 3rd person based cameras. User has 2 guns:

* A gravity gun - Picks up, drops and launches held objects
* Automatic gun - Click to fire gun and r to reload gun.
* Animations for the automatic gun were created by myself whilst animations for the character were from asset pack and I simply setup the animations to work with the model.
* Code was simple a walking script and also the following scripts: bullet fire/gravity gun/switch gun. 
